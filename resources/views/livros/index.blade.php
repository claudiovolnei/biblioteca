@extends('adminlte::page')

@section('title', 'Agenda')

@section('content_header')
<h1><i class="fas fa-address-book"></i> Lista de Livros</h1>
@stop

@section('content')

@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script> 
    $(document).ready( function () {
        $("#tabela").DataTable();
    });
</script>
@stop