<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livros extends Model
{
    protected $TABLE = "livros";
    protected $fillable = [
        'titulo',
        'nome_editora',
        'tipo_livro',
        'emprestado',
        
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
